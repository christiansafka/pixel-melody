$(document).ready(function(){
	var currentFile = null;
	if (!(window.File && window.FileReader && window.FileList && window.Blob)) {
		$("#FileDrop #Text").text("Reading files not supported by this browser");
	} else {
		$(".container").css('transition', 'all 2s');
		$("#FileDrop").on("dragenter", function(){
			$(this).addClass("Hover");
		}).on("dragleave drop", function(){
			$(this).removeClass("Hover");
		}).on("mouseenter", function() {
			$(this).addClass("mouseOver");
		}).on("mouseleave", function() {
			$(this).removeClass("mouseOver");
		});
		$("#FileDrop input").on("change", function(e){
			//get the files
			var files = e.target.files;
			if (files.length > 0){
				var file = files[0];
				$("#FileDrop #Text").text(file.name);
				currentFile = file;
				parseFile(file);
			}
		});
	}
	var options = {
		PPQ : 96,
		midiNote : true,
		noteName : true,
		velocity : true,
		duration : true
	}
	function parseFile(file){
		//get the settings
		$("#Settings input").each(function(i, e){
			if (i === 0){
				options.PPQ = parseInt(e.value);
			} else if (i === 1){
				options.midiNote = e.checked;
			} else if (i === 2){
				options.noteName = e.checked;
			} else if (i === 3){
				options.velocity = e.checked;
			} else if (i === 4){
				options.duration = e.checked;
			}
		});
		//read the file
		var reader = new FileReader();
		reader.onload = function(e){
			var transportData = MidiConvert.parseTransport(e.target.result);
			$("#TransportOutput").val(JSON.stringify(transportData, undefined, 2))
			var partsData = MidiConvert.parseParts(e.target.result, options);
			var noteArray = [];

			for(var i = 0; i < partsData[0].length; i++)
			{
				noteArray.push({"midiNote" : partsData[0][i].midiNote,
								"noteName" : partsData[0][i].noteName,
								"velocity" : partsData[0][i].velocity,
								"duration" : partsData[0][i].duration.split("i")[0]});
			}

			$("#PartsOutput").val(JSON.stringify(partsData, undefined, 2))
			var imgOptions = {
				colors: {
					"C" : "#FF0000",
					"D" : "#FF7F00",
					"E" : "#FFFF00",
					"F" : "#00FF00",
					"G" : "#0000FF",
					"A" : "#4B0082",
					"B" : "#9400D3"
				}
			}
			mapPixels(noteArray, imgOptions);
		}
		reader.readAsBinaryString(file);
	}
});