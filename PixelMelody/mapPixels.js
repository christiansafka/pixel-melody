// "never attribute to malice that which is adequately explained by stupidity"

function mapPixels(noteArray, options) {
	var canvas = document.getElementById("canvas");
	var context = canvas.getContext("2d");
 
	var transparency = 1;

	var eachSide = Math.sqrt(noteArray.length);

	$("canvas").attr("width", eachSide);
	$("canvas").attr("height", eachSide);

	//Zoomed in red 'square'
	context.fillStyle = 'rgb(255,221,' + transparency + ')';
	console.log(noteArray);
	console.log(eachSide);

	var currentPixel = 0;
	for(var i = 0; i<eachSide; i++) {
		for(var j = 0; j<eachSide; j++) {  
			var theNote = noteArray[currentPixel].noteName.split('');
			
			
			if(theNote[0] == "C") {
				context.fillStyle = options.colors.C
				context.fillRect(j, i, 1, 1);
			}
			else if(theNote[0] == "D") {
				context.fillStyle = options.colors.D
				context.fillRect(j, i, 1, 1);
			}
			else if(theNote[0] == "E") {
				context.fillStyle = options.colors.E
				context.fillRect(j, i, 1, 1);
			}
			else if(theNote[0] == "F") {
				context.fillStyle = options.colors.F
				context.fillRect(j, i, 1, 1);
			}
			else if(theNote[0] == "G") {
				context.fillStyle = options.colors.G
				context.fillRect(j, i, 1, 1);
			}
			else if(theNote[0] == "A") {
				context.fillStyle = options.colors.A
				context.fillRect(j, i, 1, 1);
			}
			else if(theNote[0] == "B") {
				context.fillStyle = options.colors.B
				context.fillRect(j, i, 1, 1);
			}
			else {
				context.fillStyle = "#FFF"
				context.fillRect(j, i, 1, 1);
			}
			if(currentPixel >= noteArray.length - 1)
			{
				return;
			}
			//context.fillRect(j, i, 1, 1);
			currentPixel += 1;
		}

		j = 0;
	}
}