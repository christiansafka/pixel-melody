# Create art from MIDI files.  

Each pixel in the image is representing a note.  The color is dependent of the note.

## Usage 

 - Clone repository
 - Open index.html
 - Drag/upload midi file to website
